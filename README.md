# Individual Project 4

[Demo Video](https://gitlab.com/kl478/individual4/-/blob/main/demo.mov)

## Project Description
This project comprises two AWS Lambda functions developed in Rust to form a data processing workflow managed by AWS Step Functions. The first function, `aggregate_data`, calculates the average score from user inputs, while the second function, `filter_data`, assesses if the average score meets a specific threshold. This automated pipeline demonstrates a practical implementation of serverless computing for data analysis tasks.

## Goals
- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline

## Project Setup

### Create Rust Lambda Project
1. Use `cargo lambda new <PROJECT_NAME>` to create lambda project.
2. Add necessary dependencies to `Cargo.toml`.
3. Implement functions in `main.rs`.
4. Create the second lambda function using the steps from 1 to 3 above. 

## Lambda Functions

### Aggregate Data Function

**Functionality**:
- Accepts JSON input with user scores.
- Computes the total and average scores.
- Outputs the number of users and the average score.

### Local Testing
1. Navigate to the `aggregate_data` directory:
   ```bash
   cd aggregate_data
   ```
2. Start the local Lambda runtime emulator:
   ```bash
   cargo lambda watch
   ```
3. Invoke the function locally using the provided sample event:
   ```bash
   cargo lambda invoke --data-file aggregate_data_event.json
   ``` 

### Filter Data Function

**Functionality**:
- Receives JSON input with the average score.
- Checks if the score is above a threshold (e.g., 50).
- Outputs whether the criteria are met along with a status message.


**Local Testing**:
1. Navigate to the `filter_data` directory:
   ```bash
   cd filter_data
   ```
2. Start the local Lambda runtime emulator:
   ```bash
   cargo lambda watch
   ```
3. Invoke the function locally using the provided sample event:
   ```bash
   cargo lambda invoke --data-file filter_data_event.json
   ```

### Deploy on AWS Lambda
1. Create a role and add the following dependencies to this role: 
    - `AWSLambda_FullAccess`
    - `AWSLambdaBasicExecutionRole`
    - `IAMFullAccess` 
2. Build the lambda function using:
    ```bash
    cargo lambda build --release --arm64
    ```
3. Deploy the lambda function to AWS Lambda using:
    ```bash
    cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>
    ```
4. Repeat the process for the second lambda function. 

### Build Step Functions
1. Create a new State Machine in AWS Step Functions. 

2. Implement the workflow of the step function by choosing AWS Lambda Invoke and drag it to the state machine with the order of the two function.

#### State Machine definition:

```json
{
  "Comment": "A description of my state machine",
  "StartAt": "Aggregate Data",
  "States": {
    "Aggregate Data": {
      "Type": "Task",
      "Resource": "arn:aws:states:::lambda:invoke",
      "OutputPath": "$.Payload",
      "Parameters": {
        "Payload.$": "$",
        "FunctionName": "arn:aws:lambda:<region>:<account_id>:function:aggregate_data:$LATEST"
      },
      "Retry": [
        {
          "ErrorEquals": [
            "Lambda.ServiceException",
            "Lambda.AWSLambdaException",
            "Lambda.SdkClientException",
            "Lambda.TooManyRequestsException"
          ],
          "IntervalSeconds": 1,
          "MaxAttempts": 3,
          "BackoffRate": 2
        }
      ],
      "Next": "Filter Data"
    },
    "Filter Data": {
      "Type": "Task",
      "Resource": "arn:aws:states:::lambda:invoke",
      "OutputPath": "$.Payload",
      "Parameters": {
        "Payload.$": "$",
        "FunctionName": "arn:aws:lambda:<region>:<account_id>:function:filter_data:$LATEST"
      },
      "Retry": [
        {
          "ErrorEquals": [
            "Lambda.ServiceException",
            "Lambda.AWSLambdaException",
            "Lambda.SdkClientException",
            "Lambda.TooManyRequestsException"
          ],
          "IntervalSeconds": 1,
          "MaxAttempts": 3,
          "BackoffRate": 2
        }
      ],
      "End": true
    }
  }
}
```

3. Validate the state machine by inputting a JSON payload to simulate a typical input scenario


## Deliverables
### Local Tests 
![test 1](screenshots/test1.png)
![test 2](screenshots/test2.png)

### AWS Lambda Functions
![lambda1](screenshots/lambda1.png)
![lambda2](screenshots/lambda2.png)

### State Machine
![State Machine](screenshots/stateMachine.png)

### State Machine Execution
![Execution1](screenshots/Execution1.png)
![Execution2](screenshots/Execution2.png)
![Execution3](screenshots/Execution3.png)
![Execution4](screenshots/Execution4.png)
![Execution5](screenshots/Execution5.png)

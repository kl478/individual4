use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

#[derive(Deserialize)]
struct Input {
    users: Vec<UserData>,
}

#[derive(Deserialize)]
struct UserData {
    // user_id: String,
    score: f32,
}

#[derive(Serialize)]
struct AggregateOutput {
    user_count: usize,
    average_score: f32,
}

async fn aggregate_data(event: LambdaEvent<Value>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    let input: Input = serde_json::from_value(event).expect("Error parsing input");
    let total_users = input.users.len();
    let total_score: f32 = input.users.iter().map(|u| u.score).sum();
    let average_score = if total_users > 0 {
        total_score / total_users as f32
    } else {
        0.0
    };

    let response = json!({
        "user_count": total_users,
        "average_score": average_score,
    });

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(aggregate_data);
    lambda_runtime::run(func).await?;
    Ok(())
}
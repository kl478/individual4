use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

#[derive(Deserialize)]
struct Input {
    average_score: f32,
}

#[derive(Serialize)]
struct FilterOutput {
    status: String,
    filtered: bool,
}

// Update the function signature to use LambdaEvent<Value>
async fn filter_data(event: LambdaEvent<Value>) -> Result<Value, Error> {
    // Decompose the LambdaEvent into its two parts
    let (event, _context) = event.into_parts();
    
    // The rest of your code remains the same
    let input: Input = serde_json::from_value(event).expect("Error parsing input");
    
    let filtered = input.average_score > 50.0;  // average score must be greater than 50
    let status = if filtered {
        "Data meets criteria"
    } else {
        "Data does not meet criteria"
    };

    let response = json!({
        "status": status,
        "filtered": filtered,
    });

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Replace handler_fn with service_fn
    let func = service_fn(filter_data);
    lambda_runtime::run(func).await?;
    Ok(())
}
